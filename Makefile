CFLAGS=`pkg-config  --cflags librsvg-2.0 libxml-2.0 libgnomecanvas-2.0` -Wall -g
LIBS=`pkg-config  --libs librsvg-2.0 libxml-2.0 libgnomecanvas-2.0`

OBJS= \
	greeter_parser.o		\
	greeter_item.o			\
	greeter_events.o		\
	greeter_item_capslock.o		\
	greeter_item_clock.o		\
	greeter_item_pam.o		\
	greeter_action_language.o	\
	greeter.o

greeter: ${OBJS}
	gcc -o greeter ${LIBS} ${OBJS}

clean:
	rm -f *o *~ greeter
